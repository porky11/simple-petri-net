(defpackage #:petri-net
  (:use #:cl #:alexandria)
  (:export #:with-new-petri-net
           #:with-petri-net

           #:add-path
           #:add-parallel
           #:add-start
           #:add-alias
           #:add-alias-before
           #:add-alias-after

           #:transition-callable-p
           #:call-transition
           #:list-active-transitions

           #:transition-uncallable-p
           #:uncall-transition
           #:list-current-transitions

           #:list-alias-transitions
           #:list-transitions
           #:list-dead-ends
           
           #:get-error-message
           #:show-petri-net
           #:write-binary))
(in-package #:petri-net)

;;;structs

(defparameter *id* 0)

(defstruct place
  in
  out
  corrupted
  (id (incf *id*))
  binary-id)

(defstruct transition
  name
  in
  out
  after
  ;before
  before-alias
  after-alias
  (count 0)
  (init-count 0)
  binary-id)

(defun place-count (place)
    (- (+ (reduce #'+ (place-in place) :key #'transition-count :initial-value 0)
          (reduce #'+ (place-in place) :key #'transition-init-count :initial-value 0))
       (reduce #'+ (place-out place) :key #'transition-count :initial-value 0)))

;;global state

(defvar *transitions*)

(defvar *corrupted*)

(defun get-error-message ()
  (when *corrupted*
    (format nil
            "Petri net in invalid state~%
~@[You may want to make the following transitions parallel:~%~{~a~%~}~]
~@[You can also set the same conditions for all of these transitions:~%~{~a~%~}~]
Adding new transitions between some transitions may also be a solution.
"
            (remove ""
                    (remove-duplicates
                     (mappend
                      (let (handled)
                        (lambda (p)
                          (split-sequence:split-sequence
                           #\Newline
                           (with-output-to-string (stream)
                             (dolist (p0 (place-corrupted p))
                               (dolist (pt (place-out p))
                                 (dolist (pt0 (place-out p0))
                                   (unless (member pt0 handled)
                                     (let* ((befores (intersection (place-in p) (place-in p0)))
                                            (afters (delete-if (lambda (a) (parallelp a pt pt0))
                                                               befores)))
                                       (when afters
                                         (format stream "~{~a~^|~}/~a&~a~%"
                                                 (mapcar #'transition-name afters)
                                                 (transition-name pt)
                                                 (transition-name pt0)))))
                                   (push pt handled))))))))
                      *corrupted*)
                     :test #'string=)
                    :test #'string=)
            (remove ""
                    (remove-duplicates
                     (mappend
                      (lambda (p)
                        (split-sequence:split-sequence
                         #\Newline
                         (with-output-to-string (stream)
                           (dolist (p0 (place-corrupted p))
                             (let ((add (set-difference (place-out p0) (place-out p)))
                                   (sub (set-difference (place-in p) (place-in p0))))
                               (when (and add sub)
                                 ;;TODO: only display soft corruptions
                                 (format stream "~{~a~^|~}: ~{~a~^|~}~%"
                                         (mapcar #'transition-name add)
                                         (mapcar #'transition-name sub))))))))
                      *corrupted*)
                     :test #'string=)
                    :test #'string=))))

(defmacro with-petri-net (net &body body)
  `(let ((*transitions* ,net)
         *corrupted*)
     ,@body
     (if *corrupted*
         (error "~a" (get-error-message))
         *transitions*)))

(defmacro with-new-petri-net (&body body)
  `(with-petri-net (make-hash-table :test #'equal)
     ,@body))


;;;helpers

(defun set= (s1 s2 &rest args)
  (= (length (apply #'intersection s1 s2 args)) (length s1) (length s2)))

(defun get-transition (transition)
  (cond ((transition-p transition)
         transition)
        ((stringp transition)
         (ensure-gethash transition *transitions*
                         (let ((transition (make-transition :name transition :before-alias nil :after-alias nil))
                               (in (make-place)))
                           (set-input-place transition in)
                           ;;(set-output-place transition out)
                           transition)))
        (t (error "Transitions can only be identified by strings"))))

(defun set-input-place (transition place)
  (pushnew place (transition-in transition))
  (pushnew transition (place-out place)))

(defun set-output-place (transition place)
  (pushnew place (transition-out transition))
  (pushnew transition (place-in place)))

(defun unset-input-place (transition place)
  (deletef (transition-in transition)  place)
  (deletef (place-out place) transition)
  (unless (place-out place)
    (remove-place place)))

(defun unset-output-place (transition place)
  (deletef (transition-out transition)  place)
  (deletef (place-in place) transition))

(defun set-corrupted-place (place place0)
  (pushnew place (place-corrupted place0))
  (pushnew place0 (place-corrupted place)))

(defun unset-corrupted-place (place place0)
  (deletef (place-corrupted place0) place)
  (deletef (place-corrupted place) place0))

;;place transform

(defun seperate-place (place ts)
  (let ((new-place (make-place)))
    (dolist (transition (place-in place))
      (set-output-place transition new-place))
    (dolist (cp (place-corrupted place))
      (corrupt cp new-place))
    (set-input-place ts new-place)
    (unset-input-place ts place)
    new-place))

(defun seperate-places (place &aux result)
  (dolist (ts (place-out place))
    (push (seperate-place place ts) result))
  (remove-place place)
  result)

(defun remove-place (place)
  (dolist (cp (place-corrupted place))
    (uncorrupt cp place))
  (dolist (transition (place-in place))
    (unset-output-place transition place))
  (dolist (transition (place-out place))
    (unset-input-place transition place)))

(defun multiply-place (n place &aux result)
  (declare (type (integer 1 *) n))
  (dotimes (i n)
    (let ((new-place (make-place)))
      (push new-place result)
      (dolist (cp (place-corrupted place))
        (corrupt cp new-place))
      (dolist (transition (place-in place))
        (set-output-place transition new-place))
      (dolist (transition (place-out place))
        (set-input-place transition new-place))))
  result)

(defun place-parallel-p (after p p0)
  (with-slots (out) p
    (with-slots ((out0 out)) p0
      (let ((pout (set-difference out out0))
            (p0out (set-difference out0 out)))
        (dolist (ts pout)
          (dolist (ts0 p0out)
            (when (parallelp after ts ts0)
              (return-from place-parallel-p t)))))))
  nil)

(defun place-alternative-p (after p p0)
  (with-slots (out) p
    (with-slots ((out0 out)) p0
      (let ((pout (set-difference out out0))
            (p0out (set-difference out0 out)))
        (dolist (ts pout)
          (dolist (ts0 p0out)
            (unless (parallelp after ts ts0)
              (return-from place-alternative-p t)))))))
  nil)

    


(defun corrupt (p p0)
  #+nil
  (warn "Temporary created invalid state of petri net")
  #+(or)
  (format t "corrupt ~a ~a~%" (place-id p) (place-id p0))
  (set-corrupted-place p p0)
  (pushnew p *corrupted*)
  (pushnew p0 *corrupted*))

(defun uncorrupt (p p0)
  #+(or)
  (format t "uncorrupt ~a ~a~%" (place-id p) (place-id p0))
  (unset-corrupted-place p p0)
  #+(or)
  (when (or (place-corrupted p)
            (place-corrupted p0))
    #+(or)
    (format t "Still corrputed: ~a: ~a; ~a: ~a~%"
            (place-id p) (mapcar #'place-id (place-corrupted p))
            (place-id p0) (mapcar #'place-id (place-corrupted p0))))
  (unless (place-corrupted p)
    (deletef *corrupted* p))
  (unless (place-corrupted p0)
    (deletef *corrupted* p0)))

(defun unify-places (p p0)
  ;;(show-petri-net :program "viewnior")
  (with-slots (in out) p
    (with-slots ((in0 in) (out0 out)) p0
      (when (set= in in0)
        (let ((pout (set-difference out out0))
              (pout0 (set-difference out0 out)))
          (dolist (pt pout)
            (dolist (pt0 pout0)
              (dolist (after in)
                (unless (parallelp after pt pt0)
                  #+(or)
                  (format t "Connect ~a and ~a~%"
                          (transition-name pt)
                          (transition-name pt0))
                  ;;(show-petri-net :program "viewnior")
                  (when (every (lambda (transition) (not (parallelp after pt transition)))
                               out0)
                    (set-input-place pt p0))
                  (when (every (lambda (transition) (not (parallelp after pt0 transition)))
                               out)
                    (set-input-place pt0 p))))))))
      ;;(format t "out=~a~%in=~a~%" (set= out out0) (set= in in0))
      ;(show-petri-net :program "viewnior")
      (if (set= out out0)
          (if (set= in in0)
              (progn
                (remove-place p0)
                t)
              (progn
                (when (and (subsetp in in0) (not (subsetp in0 in)))
                  (remove-place p0)
                  (return-from unify-places t))
                (when (and (subsetp in0 in) (not (subsetp in in0)))
                  (remove-place p)
                  (return-from unify-places t))))
          (if (set= in in0)
              (progn
                (when (and (subsetp out out0) (not (subsetp out0 out)))
                  (remove-place p)
                  (return-from unify-places t))
                (when (and (subsetp out0 out) (not (subsetp out out0)))
                  (remove-place p0)
                  (return-from unify-places t)))
              (when (every (lambda (after) (or (place-alternative-p after p p0)
                                               (not (place-parallel-p after p p0))))
                           (intersection in in0))
                ;;TODO: both conditions are different kinds of corruptions and need different error messages.
                ;;For example, when places a and b are parallel, but are alternative either,
                ;;makign them parallel using a&b won't help
                (corrupt p p0)
                nil))))))




(defun compute-parallels (ts)
  #+(or)
  (format t "compute parallels ~a~%" (transition-name ts))
  (dolist (place (transition-in ts))
    (seperate-places place))

  #+nil
  (dolist (place (transition-in ts))
    (dolist (pts (place-out place))
      (when (member pts (transition-parallels ts))
        (seperate-place place pts))))
  ;;(format t "frehshly seperated before ~a~%" (transition-name ts))
  ;;(show-petri-net :program "viewnior")

  (loop named rim
     do (block repeat
          (dolist (bef (remove-duplicates (mappend #'place-in (transition-in ts))))
            (loop for (p . ps0) on (transition-out bef)
               do (dolist (p0 ps0)
                    ;;(show-petri-net :program "viewnior")
                    (when (unify-places p p0)
                      (return-from repeat)))))
          (return-from rim))))

(defun append-lists (lists)
  (remove-duplicates
   (reduce (lambda (first second)
             (mappend (lambda (first)
                       (mapcar (lambda (second)
                                 (append first second))
                               second))
                     first))
           lists)))

(defmacro prl (name)
  `(format t "~a: ~S~%" ',name ,name))

(defun before-alias (transition)
  (let* ((alias-list (transition-before-alias transition))
         (alias-or-list
          (mappend (lambda (alias-and-list)
                     (append-lists (mapcar #'before-alias alias-and-list)))
                   alias-list)))
    (cons (list transition) alias-or-list)))

(defun after-alias (transition)
  (let* ((alias-list (transition-after-alias transition))
         (alias-or-list
          (mappend (lambda (alias-and-list)
                     (append-lists (mapcar #'after-alias alias-and-list)))
                   alias-list)))
  (cons (list transition) alias-or-list)))


(defun add-parallel (after ts ts0)
  (let ((after (get-transition after))
        (ts (get-transition ts))
        (ts0 (get-transition ts0)))
    (dolist (after (flatten (after-alias after)))
      (let ((tss (after-alias ts))
            (tss0 (after-alias ts0)))
        ;(format t "s ~S~%" (mapcar (lambda (ts) (mapcar #'transition-name ts)) tss))
        ;(format t "s0 ~S~%" (mapcar (lambda (ts) (mapcar #'transition-name ts)) tss0))
        (dolist (tss tss)
          (loop for (ts . tss0) on tss
             do (dolist (ts0 tss0)
                  (add-parallel-simple after ts ts0))))
        (dolist (tss0 tss0)
          (loop for (ts . tss0) on tss0
             do (dolist (ts0 tss0)
                  (add-parallel-simple after ts ts0))))
        (dolist (ts (flatten tss))
          (dolist (ts0 (flatten tss0))
            (add-parallel-simple after ts ts0)))))))

(defun add-parallel-simple (after ts ts0)
  #+(or)
  (format t "Adding parallel ~a/~a&~a~%"
          (transition-name after)
          (transition-name ts)
          (transition-name ts0))
  ;;(show-petri-net :program "viewnior")
  (pushnew (list ts ts0) (transition-after after) :test #'set=)
  ;(pushnew (cons after ts) (transition-before ts0) :test #'equal)
  ;(pushnew (cons after ts0) (transition-before ts) :test #'equal)
  (dolist (p (transition-in ts))
    (dolist (p0 (transition-in ts0))
      (uncorrupt p p0)))
  (loop for (p . ps0) on (transition-out after)
     do (dolist (p0 ps0)
          (uncorrupt p p0)))
  (dolist (place (transition-in ts))
    (seperate-places place))
  (dolist (place (transition-in ts0))
    (seperate-places place))

  ;;(maphash (lambda (key value) (compute-parallels value)) *transitions*)
  (compute-parallels ts)
  (compute-parallels ts0)
  )

(defun parallelp (after ts ts0)
  (member (list ts ts0) (transition-after after) :test #'set=))
             
(defun restruct (list)
  (loop
     while list
     collect
       (loop
          for sublist in list
          if sublist
          collect (car sublist) into elements
          if (cdr sublist)
          collect (cdr sublist) into restlist
          finally (progn
                    (setf list restlist)
                    (return elements)))))


(defmethod print-object ((object place) stream)
  (format stream "#_~S" (place-id object)))

(defmethod print-object ((object transition) stream)
  (format stream "#_~S" (transition-name object)))

#+(or)
(set-dispatch-macro-character #\# #\_ (lambda (stream &rest rest)
                                        (declare (ignore rest))
                                        (let ((s (read stream t)))
                                          (etypecase s
                                            (string `(get-transition ,s))
                                            (integer (error "Cannot get places"))))))

(defun add-path (transition before-transitions)
  ;(format t "Trying to add ~a after ~{~a~^ and ~}~%" transition before-transitions)
  (let* ((transition (get-transition transition))
         (before-transitions (delete-duplicates
                              (mapcar (lambda (id) (get-transition id))
                                      before-transitions))))
    (dolist (transitions (before-alias transition))
      (dolist (before-transitions (append-lists (mapcar #'after-alias before-transitions)))
        ;(format t "bef ~a~%" before-transitions)
        (dolist (after before-transitions)
          ;(format t "here ~a~%" transitions)
          (loop for (transition . transitions) on transitions
             do (dolist (transition0 transitions)
                  (add-parallel after transition transition0))))
        ;(format t "there~%")
        (dolist (transition transitions)
          #+(or)
          (format t "~a: ~{~a~^&~}~%" (transition-name transition) (mapcar #'transition-name before-transitions))
          (let* ((before-length (length before-transitions))
                 (ins (mapcar (lambda (place) (seperate-place place transition)) (transition-in transition))))
            (let ((inlist
                   (mapcar (lambda (place)
                             (prog1
                                 (multiply-place before-length place)
                               (remove-place place)))
                           ins)))
              (let ((inlist (restruct inlist)))
                (assert (= (length inlist) (length before-transitions))
                        (inlist before-transitions) "not equal len")
                (loop
                   for insub in inlist
                   for bef in before-transitions
                   do (loop
                         for in in insub
                         do (set-output-place bef in)))))
            (compute-parallels transition)))))))

(defmacro ign (&rest rest)
  (declare (ignore rest)))

(defun add-start (transition count)
  (symbol-macrolet ((tc (transition-init-count (get-transition transition))))
    (setf tc (max tc count))))

(defun has-before-alias (this others)
  (member this (flatten (mapcar #'before-alias others))))

(defun add-alias-before (transition transitions)
  ;;(write-line "calling this")
  (let ((transition (get-transition transition))
        (ts (mapcar #'get-transition transitions)))
    (when (has-before-alias transition ts)
      (error "Cyclic before alias definition"))
    (pushnew ts (transition-before-alias transition) :test #'set=)
    ;;(format t "after ~a~%" transition)
    (dolist (after (flatten (mapcar #'place-in (transition-in transition))))
      (loop for (t0 . ts1) on ts
         do (dolist (t1 ts1)
              (add-parallel after t0 t1))))
    ;;(format t "after ~a~%" ts)
    (dolist (transition0 (flatten (mapcar #'before-alias ts)))
      (set-equal-befores transition transition0))))

(defun aliasp (transition)
  (let ((transition (get-transition transition)))
    (or
     (> (transition-init-count transition) 0)
     (transition-before-alias transition)
     (transition-after-alias transition))))

(defun has-after-alias (this others)
  (member this (flatten (mapcar #'after-alias others))))

(defun add-alias-after (transition transitions)
  (let ((transition (get-transition transition))
        (ts (mapcar #'get-transition transitions)))
    (when (has-before-alias transition ts)
      (error "Cyclic after alias definition"))
    (pushnew ts (transition-after-alias transition) :test #'set=)
    (set-equal-afters transition (flatten (mapcar #'after-alias ts)))))

(defun add-alias (transition befores afters)
  (add-alias-before transition befores)
  (add-alias-after transition afters))


(defun set-equal-afters (from tos)
  (dolist (par (transition-after from))
    (destructuring-bind (p0 p1) par
      (dolist (to tos)
        (add-parallel to p0 p1))))
  (let ((froms (transition-out from)))
    (let ((tolen (length tos)))
      (let ((places
             (mapcar (lambda (place)
                       (prog1 (multiply-place tolen place)
                         (remove-place place)))
                     froms)))
        (dolist (outs places)
          (loop for to in tos
             for out in outs
             do (set-output-place to out))
          (loop for (place . places) on outs
             do (dolist (place0 places)
                  (unify-places place place0))))
        (dolist (to tos)
          (loop for (place . places) on (transition-out to)
             do (dolist (place0 places)
                  (unify-places place place0))))))))


(defun set-equal-befores (from to)
  ;;TODO: This is incorrect
  ;;Example:
  #|
  Example:
  This works:
  ```
  a=b
  b: x
  ```
  This works not:
  ```
  b: x
  a=b
  ```

  |#
  (let ((froms (transition-in from))
        (tos (transition-in to)))
    (let ((fromlen (length froms))
          (tolen (length tos)))
      (let ((all-froms
             (mapcar (lambda (place)
                       (prog1 (multiply-place tolen place)
                         (remove-place place)))
                     froms))
            (all-tos
             (restruct
              (mapcar (lambda (place)
                        (prog1 (multiply-place fromlen place)
                          (remove-place place)))
                      tos))))
        (loop
           for some-froms in all-froms
           for some-tos in all-tos
           do (loop
                 for from-place in some-froms
                 for to-place in some-tos
                 do (progn
                      (dolist (bef (place-in from-place))
                        (set-output-place bef to-place))
                      ;;TODO: this fixes the above bug, but will also cause in different results
                      #+(or)
                      (dolist (bef (place-in to-place))
                        (set-output-place bef from-place))
                      (unify-places from-place to-place)
                      ;;TODO: this may be a weird hack, but at least it seems to work for now
                      (uncorrupt from-place to-place)
                      )))))))

(defun delete-transition (ts)
  (dolist (in (transition-in ts))
    (unset-input-place ts in))
  (dolist (out (transition-out ts))
    (unset-output-place ts out))
  (remhash (transition-name ts) *transitions*))


;;;execute

(defun transition-callable-p (transition)
  (let ((ts (get-transition transition)))
    ;;(format t "~a object: ~a~%" ts (transition-p ts))
    (and
     (not *corrupted*)
     (transition-p ts)
     (not (transition-before-alias ts))
     (not (transition-after-alias ts))
     (every (lambda (place)
              ;;(format t "place-count: ~a~%" (place-count place))
              (< 0 (place-count place)))
            (transition-in ts)))))

(defun transition-uncallable-p (transition)
  (let ((ts (get-transition transition)))
    (and (not *corrupted*)
         (transition-p ts)
         (< 0 (transition-count ts))
         (every (lambda (place)
                  ;;(format t "place-count: ~a~%" (place-count place))
                  (< 0 (place-count place)))
                (transition-out ts)))))

(defun call-transition (transition)
  (assert (not *corrupted*) (*corrupted*)
          "~a" (get-error-message))
  (assert (transition-callable-p transition) (transition)
          "Transition ~S cannot be called" (transition-name (get-transition transition)))
  (incf (transition-count (get-transition transition))))

(defun uncall-transition (transition)
  (assert (not *corrupted*) (*corrupted*)
          "~a" (get-error-message))
  (assert (transition-uncallable-p transition) (transition)
          "Transition ~S cannot be uncalled" (transition-name (get-transition transition)))
  (decf (transition-count (get-transition transition))))

(defun list-active-transitions ()
  (loop for transition being the hash-key of *transitions*
     if (transition-callable-p transition)
     collect transition))

(defun list-current-transitions ()
  (loop for transition being the hash-key of *transitions*
     if (transition-uncallable-p transition)
     collect transition))

(defun list-alias-transitions ()
  (loop for transition being the hash-key of *transitions*
     if (aliasp transition)
     collect transition))

(defun list-transitions ()
  (loop for transition being the hash-key of *transitions*
     if (not (aliasp transition))
     collect transition))

(defun list-dead-ends ()
  (loop for transition being the hash-key of *transitions*
     if (not (or (aliasp transition) (transition-out (gethash transition *transitions*))))
     collect transition))

;;show the petri net using graphviz

(defun before-name (place &optional place-id)
  (let ((result (format nil "\"~a\" [label=\"~a\" fillcolor=~:[red~;blue~] style=filled]"
                        ;;(mapcar #'transition-name (place-in place))
                        (place-id place)
                        (if place-id
                          (place-id place)
                          (place-count place))
                        ;#+nil
                        (< 0 (place-count place)))))
    result))

(defun show-petri-net (&key program allow-corrupted print-id active-only show-alias only size (type "png") (path (if program (format nil "~apetri-net" *default-pathname-defaults*) "petri-net")))
  (when (or allow-corrupted (not *corrupted*))
    (with-open-file (stream "petri-net.dot" :if-does-not-exist :create :if-exists :supersede :direction :output)
      (format stream "digraph {~%rankdir=BT~%")
      (let ((active (list-active-transitions))
            (current (list-current-transitions))
            (alias (list-alias-transitions)))
        (when only (setf active (intersection only active :test #'string=)
                         current (intersection only current :test #'string=)))
        (format stream "~:[~;~{~S~^, ~}[fillcolor=green style=filled]~%~]"
                active active)
        (format stream "~:[~;~{~S~^, ~}[fillcolor=violet style=filled]~%~]"
                current current)
        (when show-alias
          (format stream "~:[~;~{~S~^, ~}[fillcolor=orange style=filled]~%~]"
                  alias alias)))
      (let (first)
        (maphash (lambda (key transition)
                   (when (and (transition-p transition)
                              (or show-alias (not (aliasp transition)))
                              (or (not only) (member key only :test #'string=)))
                     (when (or (not active-only) (or (transition-callable-p transition)
                                                     (transition-uncallable-p transition)))
                       (if first
                           (format stream ", ")
                           (setq first t))
                       (format stream "~S" key))))
                 *transitions*)
        (when first
          (format stream "[shape=box]~%")))
      (maphash (lambda (key transition)
                 (when (and (transition-p transition)
                              (or show-alias (not (aliasp transition)))
                              (or (not only) (member key only :test #'string=)))
                   (when (or (not active-only) (or (transition-callable-p transition)
                                                   (transition-uncallable-p transition)))
                     (flet ((identify-place (place)
                              (before-name place print-id)))
                       (format stream "{~{~a~^ ~}}->\"~a\"->{~{~a~^ ~}}~%"
                               (mapcar #'identify-place(petri-net::transition-in transition))
                               key
                               (mapcar #'identify-place (petri-net::transition-out transition)))))))
               *transitions*)
      (format stream "}~%"))
    (if size
        (inferior-shell:run/nil
         (format nil "dot -T~a -Gsize=~a,~a\! -Gdpi=1 petri-net.dot -o ~a.~a"
                 type
                 (aref size 0)
                 (aref size 1)
                 path
                 type))
        (inferior-shell:run/nil (format nil "dot -T~a petri-net.dot -o ~a.~a" type path type)))
    (when program
      (inferior-shell:run/nil (format nil "~a ~a.~a" program path type)))))

(defun write-binary (&key net keys all)
  (unless *corrupted*
    (when all
      (setf net (format nil "~a.pns" all)
            keys (format nil "~a.pnk" all)))
    (let (transitions places (tc 0) (p 0))
      (maphash (lambda (key transition)
                 (when (and (transition-p transition)
                            (not (aliasp transition)))
                   (push transition transitions)
                   (dolist (place (transition-in transition))
                     (pushnew place places))
                   (dolist (place (transition-out transition))
                     (pushnew place places))))
               *transitions*)
      (setf places (reverse places))
      (dolist (place places)
        (setf (place-binary-id place) p)
        (incf p))
      (dolist (transition (reverse transitions))
        (setf (transition-binary-id transition) tc)
        (incf tc))
      (let* ((transition-list
              (sort
               (let (keylist)
                 (maphash (lambda (key transition)
                            (when (and (transition-p transition)
                                       (not (aliasp transition)))
                              (push (cons key transition) keylist)))
                          *transitions*)
                 keylist)
               #'<
               :key (lambda (cons) (transition-binary-id (cdr cons)))))
             (keylist (mapcar #'car transition-list))
             (sorted-transitions (mapcar #'cdr transition-list)))
        (when net
          (with-open-file (stream net :if-does-not-exist :create :if-exists :supersede :direction :output :element-type '(signed-byte 32))
            (labels
                ((w (a)
                   (write-byte a stream)))
              (w p)
              (dolist (place (sort places #'< :key #'place-binary-id))
                (w (place-count place)))
              (w tc)
              (dolist (transition sorted-transitions)
                (with-slots (in out) transition
                  (let ((len (length (intersection out places))))
                    (w len)
                    (dolist (place out)
                      (when (member place places)
                        (w (place-binary-id place)))))
                  (let ((len (length (intersection in places))))
                    (w len)
                    (dolist (place in)
                      (when (member place places)
                        (w (place-binary-id place))))))))))
        (when keys
          (with-open-file (stream keys :if-does-not-exist :create :if-exists :supersede :direction :output)
            (dolist (key keylist)
              (write-line key stream)))))))
  (values))
