(defpackage #:petri-test
  (:use #:cl #:alexandria #:split-sequence #:inferior-shell
        #:petri-net #:petri-language #:petri-parser)
  (:export #:interactive
           #:with-petri-net-from-file
           #:petri-call
           #:list-undeclared
           #:generate-image
           #:generate-binary))
(in-package #:petri-test)

(defun print-active ()
  (format t "active: ~{~a~^ ~}~%" (list-active-transitions)))

(defun print-current ()
  (format t "current: ~{~a~^ ~}~%" (list-current-transitions)))

(defun callables (&rest names)
  (dolist (name names)
    (format t "~a: ~a, " name (if (transition-callable-p name) "yes" "no")))
  (format t "~%"))

(defun uncallables (&rest names)
  (dolist (name names)
    (format t "~a: ~a, " name (if (transition-uncallable-p name) "yes" "no")))
  (format t "~%"))

(defun get-file (filename)
  (with-open-file (stream filename :if-does-not-exist :create)
    (loop for line = (read-line stream nil)
       while line
       collect line)))

(defvar *whitespaces*
  '(#\Space #\Newline #\Backspace #\Tab 
    #\Linefeed #\Page #\Return #\Rubout))

(defvar *expressions*)

(defun save-petri-net (filename)
  (with-open-file (*standard-output*
                   filename
                   :direction :output
                   :if-exists :supersede)
    (dolist (expr *expressions*)
      (write-line expr))))

(defun save-test-story (filename story)
  (with-open-file (stream
                   filename
                   :direction :output
                   :if-exists :supersede)
    (maphash (lambda (key list)
               (when key
                 (format stream "~a~%~{~a~%~}~%"
                         key list)))
             story)))

(defun load-test-story (filename)
  (let ((story (make-hash-table :test #'equal))
        (list (split-sequence ""
                              (mapcar (lambda (line)
                                        (string-trim *whitespaces* line))
                                      (get-file filename))
                              :test #'equal)))
    (loop for (name . value) in list
       do (setf (gethash name story) value))
    story))

(defvar *help-message* "Write a line beginning with one of the following characters:
`?`: Show this help message
`-`: List the current transitions
`+`: List the active transitions
`:`: Write the name of a program, that shows the current state of the story (offline only)
`!`: Write an expression to modify the current story
`*`: Write the name of the files without extension to define new stories (offline only)
     Alternatively just save with some default name (also online)
`.`: Stop running (offline only)
")

(defun declare-story (name story)
  (declare (special *sn*))
  (format t "Declare story ~S~%" name)
  (finish-output)
  (loop with lines
     for line = (read-line)
     until (and lines (string= "" line))
     do (unless (string= "" line) (push line lines))
     finally (setf (gethash name story) (reverse lines)))
  (save-test-story *sn* story))


(defun generate-image (pn &rest keys)
  (with-new-petri-net
    (load-file pn)
    (apply #'show-petri-net keys)))

(defun generate-binary (pn &rest keys)
  (with-new-petri-net
    (load-file pn)
    (apply #'write-binary keys)))

(defun show-alternatives (transitions story &aux (i 0))
  (if (= 1 (length transitions))
      (write-line (or (car (gethash (car transitions) story)) (car transitions)))
      (dolist (transition transitions)
        (format t "~a: ~a~%" (incf i) (or (car (gethash transition story))
                                          transition))))
  (finish-output)
  (length transitions))

(defvar *running*)
(defvar *new-expressions*)

(defun handle-string (string story online)
  (declare (special *pn* *sn*))
  (unless (string= string "")
    (let ((expr (subseq string 1)))
      (case (char string 0)
        (#\+ (print-active))
        (#\- (print-current))
        (#\:
         (unless online
           (unless (ignore-errors (show-petri-net :program expr) t)
             (format t "Failed to open the image of the petri net using ~a~%" expr))))
        (#\!
         (when (ignore-errors (load-expression expr) t)
           (push expr *new-expressions*)
           (format t "Added new expressions~%")))
        (#\*
         (if-let ((err (get-error-message)))
           (warn err)
           (progn
             (nconcf *expressions* (reverse *new-expressions*))
             (setq *new-expressions* nil)
             (if (or online (string= expr ""))
                 (progn
                   (save-petri-net *pn*)
                   (save-test-story *sn* story)
                   (if online
                       (format t "Saved the petri net and the story")
                       (format t "Saved the petri net and the story to files ~a and ~a~%" *pn* *sn*)))
                 (progn
                   (save-petri-net (format nil "~a.pn" expr))
                   (save-test-story (format nil "~a.story" expr) story)
                   (format t "Saved the petri net and the story to files ~a.pn and ~a.story~%" expr expr))))))
        (#\.
         (unless online
           (setf *running* nil)
           (format t "Stopped running~%")))
        (#\? (format t *help-message*))
        (t (return-from handle-string nil)))
      (finish-output)
      t)))



(defun fill-stories (transitions story &aux declared)
  (dolist (ts transitions)
    (unless (gethash ts story)
      (setq declared t)
      (declare-story ts story)))
  declared)

(defun list-undeclared (pn0 &optional sn0
                        &aux
                          (pn (if sn0
                                    pn0
                                    (format nil "~a.pn" pn0)))
                          (sn (if sn0
                                    sn0
                                    (format nil "~a.story" pn0))))
  (with-new-petri-net
    ;;load file
    (load-file pn)
    (let ((story (load-test-story sn)))
      (return-from list-undeclared
        (remove nil (mapcar (lambda (key) (unless (gethash key story) key)) (list-transitions)))))))

(defmacro with-petri-net-from-file (filename &body body)
  `(with-new-petri-net
     (load-file ,filename)
     ,@body))

(defun petri-call (filename function &rest args)
  (with-petri-net-from-file filename
    (return-from petri-call (apply function args))))

(defun interactive (&optional (secure t) (pn0 (format nil "~aexample" (ql:where-is-system :simple-petri-net)))
                      sn0
                    &aux
                      (*pn* (if sn0
                                pn0
                                (format nil "~a.pn" pn0)))
                      (*sn* (if sn0
                                sn0
                                (format nil "~a.story" pn0)))
                      (*expressions* (get-file *pn*))
                      *new-expressions*
                      (*running* t))
  (declare (special *pn* *sn*))
  (with-new-petri-net
    ;;load file
    (load-file *pn*)
    (let ((story (load-test-story *sn*)))
      (loop for transitions = (list-active-transitions)
         while *running*
         ;;print all possible alternatives
         do (if (not transitions)
                (progn
                  (format t "\
You cannot do anything.
~a" *help-message*)
                  (finish-output)
                  (loop while
                       (handle-string (read-line) story secure)))
                (progn
                  (fill-stories transitions story)
                  (let ((i (show-alternatives transitions story)))
                    ;;select an alternative
                    (when (< 1 i)
                      (format t "> ")
                      (finish-output))
                    (let ((select (loop
                                     while *running*
                                     for line =  (read-line)
                                     for handle = (handle-string line story secure)
                                     if (not handle)
                                     do (let ((num
                                               (or (and (= 1 i) 1)
                                                   (parse-integer line :junk-allowed t))))
                                          (when (and (integerp num) (<= 1 num i))
                                            (return (nth (1- num) transitions)))
                                          (setq transitions (list-active-transitions))
                                          (if (fill-stories transitions story)
                                              (setq i (show-alternatives transitions story))
                                              (format t "You have to write a number from 1 to ~a~%" i))
                                          (format t "> ")
                                          (finish-output))
                                     else
                                     do (progn
                                          (format t "~%> ")
                                          (finish-output)))))
                      ;;call the selected alternative
                      (when select
                        (call-transition select)
                        (dolist (line (cdr (gethash select story)))
                          (write-line line)
                          (finish-output)
                          (loop while
                               (handle-string (read-line) story secure))))))))))))

#+nil
(with-new-petri-net (expression "(a:b)|(c:d)")
                    (add-macro "a" (list "c") (list "c"))
                    (show-petri-net :program "viewnior")
                    )
(defun all-lists (list)
  (if list
      (let ((first (ensure-list (car list))))
        (mappend (lambda (first)
                  (mapcar (lambda (rest)
                             (cons first rest))
                           (all-lists (cdr list))))
                first))
      (list nil)))
            


(defun expand-test (&optional (list '((a b c) (d e) (f)))
                      (b '((x y) (z))))
  (mapcar (lambda (alt)
            (mapcar (lambda (par)
                      (if (eq par 'b)
                          (mapcar (lambda (alt)
                                    (mappend (lambda (par)
                                               (list par))
                                             alt))
                                  b)
                          (list par)))
                    alt))
          list))
