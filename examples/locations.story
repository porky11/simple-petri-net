city teleport
You teleport to the city.

forest teleport
You teleport to the forest.

field teleport
You teleport to the field.

village teleport
You teleport to the village.

jungle teleport
You teleport to the jungle.

cave teleport
You teleport to the cave.

city
You are in the city.

city die
You die in the city.

city stay
You stay in the city.

city sleep
You sleep in the city.

city look
You look around in the city.

city to cave
You go to the cave.

cave
You are in the cave.

cave die
You die in the cave.

cave stay
You stay in the cave.

cave sleep
You sleep in the cave.

cave look
You look around in the cave.

cave to village
You go to the village.

cave to city
You go to the city.

cave to jungle
You go to the jungle.

jungle
You are in the jungle.

jungle die
You die in the jungle.

jungle stay
You stay in the jungle.

jungle sleep
You sleep in the jungle.

jungle look
You look around in the jungle.

jungle to cave
You go to the cave.

