intro
The story begins.

home
You are at home.

home to garden
You go to the garden.

home to school
You go to the school.

home grab ball
You grab a ball at your home.

home play ball
You play with the ball at home.

home throw ball
You throw the ball away at home.

school
You are at school.

school grab ball
You grab a ball at your school.

school to garden
You go to the garden.

school to home
You go home again.

school play ball
You play with the ball at school.

school throw ball
You throw the ball away at school.

garden
You are in the garden.

garden play ball
You play ball in the garden.

garden throw ball
You throw the ball away in the garden.

garden to home
You go home again.

garden to school
You go to school.

garden grab ball
You grab the ball in the garden.

