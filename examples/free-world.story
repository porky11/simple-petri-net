intro
You live in a village.

your home
You are at your home now.

leave your home
You leave your home.

village
You are in the village now.

village to forest
You leave the village to go to the forest next to it.

village to cave
You leave the village and go to a cave.
When you enter it, it gets very dark.

village to field
You leave the village to play on a wide field next to it.

look village
You look around.
There are some houses and persons in the village.

field
You stand around.

look field
You look around the field.
It is wide and empty.

field to village
The field is too empty, so you go back to the village.

forest
You are in a forest.

look forest
You look around the forest.
It's a wild and vacant forest.

forest to village
From the forest you go back to the village.

cave
It's so dark inside the cave.
You want to leave it again.

look cave
You look around.
But it's so dark, you almost don't see anything.

cave to village
You try to go back to the village.
After some slow walking you see a light.
You leave the cave and can see the village, so you go to it.

cave to city
You try to go to the city, which is on the other side of the cave.
You go slowly and some time later you see a light.
You follow the light and leave the cave.
The city is far away, but you can see it.
You walk a bit, and some time later you finally enter the city.

city
There is much to do inside the city.

look city
You look around.
There are many big buildings inside the city.

cave to jungle
You heard, you can reach a jungle from the cave.
You walk a bit around, until you hear some wild animals.
You follow the sound, and reach the animals.

jungle
You walk around in the jungle.

look jungle
You look around.
There are many big trees.
You see some animals on them.

jungle to cave
You go back to the cave you came from.

city to cave
You want to leave the city again.
Some nice person can drive you back to the cave, so you don't have to walk that far.

male person
You go to a man with a beard.
He: "I'm an adventurer"
"Lets meet again in the jungle"
He goes away

female person
You see a cute girl.
She: "I want to go to the city"
"You can do cool things there"
She goes to the city.

female in city
You go to the girl again.
She: "Hi, it's nice to be in the city"
"Let's meet again some time"
You go away again.

male in jungle
You go to the man again.
He: "I found many interesting people here"
"Come with me, so I can show you"

go with him
You follow the man.
He permanently says stupid things to you.
Finally you find some small village.

leave him
You don't go with him.

jungle village
You are in the jungle village now.

look jungle village
You look around.
The houses are very small.
You don't even know, if you should call them houses.
Most people here have a slightly dark skin and don't wear almost no clothes.

jungle village to jungle
You leave the jungle village.

jungle to jungle village
You go back to the jungle village.

register university
You go to the university.
You want to register there.
There are many subjects, you can study there.
You have to choose two of them.

choose first subject
You choose your first subject.
What should your first subject be?

choose second subject
You choose your second subject.
What should your second subject be?

finish registration
You have selected all subjects you need.
This means, your registration is finished.
You are now allowed to visit the university as often as you want.
But now you go again.

visit university
You visit the university.
There are many nice people.
But your courses are at other places, so you go again.

choose magic
You choose magic as a subject.
You will learn to use magic at the field near the village.

choose fight
You choose fight as a subject.
You will learn to fight in the forest near the village.

choose science
You choose science as a subject.
You can experiment at a laboratorium in the city.

learn magic
You wait until your magic training begins.
Some other magicans come and you learn some cool abilities.
Then the others go away again.

learn fight
You wait until your fight training begins.
Some other fighters come and you have to equip some weapons.
Then you try to fight the others while hiding behind trees.
You learn much and go back to the village afterwards

learn science
You visit the laboratorium.
Everything looks complicated so you go outside again.


