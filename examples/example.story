intro
You live at home with your mother.

stay home
You stay at home and nothing happens.

leave home
You leave home.
You are not sure what to do today.

mothers story
After you left your home, your mother is alone.
She celebrates with her friends.

visit friend
You decide to visit your friend today.
After you went to him, you both have much fun.

go away
You go away from your home.
After some walking a bit, you see some ways, you could go next.

go village
You go to the village.
But inside the village it's boring, so you go further.

go field
You go to a field.
But there is nothing, so you go further.

go forest
You go to a forest.
It's warm there, so you get naked.

go cave
You go to a dark cave.
You like it and after some time you are outside again.

go city
You go to the city, you see now far away
After some time you are there.

inside city
Inside the city live some relatives.

visit father
You visit your father.
You don't visit him often, so he is happy now.
But you leave pretty fast again.

visit uncle
You visit your uncle.
You like him very much, but he does not have much time.
You have to go again.

visit aunt
You visit your aunt.
She doesn't like you, so you have to go again.

meet girl
There is a cute girl in the city.
You go to her and talk to her.
She wants to go away with you and have some adventure.
You leave the city and go to some beach where pirates are.
There seems to be noone on the ship, so you silently go onto the ship.
She wants you to find weapons, while she searches for gold.

your adventure
You find some sleeping pirate, who has a sharp sword. You take it away and go back to the beach.

girl adventure:
She goes down some stairs and finds a big room, where some treasure chests are in. She opens one and steals as much gold she can carry. Then she goes back to the beach.

play video games
You play video games together.
This is much fun.

play outside
You play outside with your friend.
He has many ideas for games to play with you.

talk friend
You only talk to your friend.
But some of the topics he knows about are very interesting.

leave friend
Then you leave your friend again.

