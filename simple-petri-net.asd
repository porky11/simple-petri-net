(asdf:defsystem #:simple-petri-net
  :version "0.1.0"
  :author "Fabio Krapohl <fabio.u.krapohl@fau.de>"
  :description "Engine for writing stories with parallel and alternate story lines based on petri nets"
  :license "LLGPL"
  :depends-on (#:alexandria #:split-sequence #:inferior-shell)
  :serial t
  :components ((:file "net")
               (:file "language")
               (:file "parser")
               (:file "test")))


