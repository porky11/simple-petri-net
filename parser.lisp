(cl:defpackage #:petri-parser
  (:use #:cl #:petri-language #:alexandria #:split-sequence)
  (:shadow #:error)
  (:export
   #:parse-expression
   #:parse-expressions
   #:parse-file

   #:load-expression
   #:load-expressions
   #:load-file
   
   #:expression
   #:expressions
   #:file

   #:with-parsing-strings))
(in-package #:petri-parser)

(defvar *whitespaces*
  '(#\Space #\Newline #\Backspace #\Tab 
    #\Linefeed #\Page #\Return #\Rubout))

(defun split-string (string split)
  (do
   ((i 0 (1+ i))
    (count 0 (cond ((char= (char string i) #\() (1+ count))
                   ((char= (char string i) #\)) (1- count))
                   (t count))))
   ((and (= count 0) (char\= (char string i) split))
    (values (subseq string 0 i) (subseq string (1+ i))))))

(macrolet ((error (datum &rest arguments) `(cl:error ,(format nil "Parsing error: ~a" datum) ,@arguments)))

(defun parse-expression (string &aux (count 0))
  (let ((char
         (loop for chars in (list #\newline #\; (list #\: #\= #\< #\> #\/) #\| #\& #\. #\* #\()
            do (let ((char (block nil
                             (map nil (lambda (char)
                                        (when (and (= count 0)
                                                   (member char (ensure-list chars)))
                                          (return char))
                                        (when (char= char #\( )
                                          (incf count))
                                        (when (char= char #\) )
                                          (decf count)))
                                  string))))
                 (unless (= count 0)
                   (error "Brackets don't match"))
                 (when char
                   (return char))))))
    (ecase char
      ;;by default trim spaces and check for valid names
      ((nil)
         (let ((string (string-trim *whitespaces* string)))
           (if (string= string "")
               '(empty)
               (if (every (lambda (char) (or (find char " -") (alphanumericp char))) string)
                   (let ((strings (delete "" (split-sequence #\Space string)
                                          :test #'string=)))
                     (if (cdr strings)
                         `(names ,@strings)
                         (car strings)))
                   (error "Names can only contain characters, numbers, `-` and Spaces")))))
      ;;add path
      (#\: (multiple-value-bind (first second)
               (split-string string char)
             `(path ,(parse-expression first) ,(parse-expression second))))
      ;;unnamed
      (#\. (multiple-value-bind (first second)
               (split-string string char)
             (let ((first (parse-expression first))
                   (second (parse-expression second)))
               `(unnamed ,first ,second))))
      (#\*
       (let ((l* (string-trim *whitespaces* string)))
         (assert (eq (aref l* (1- (length l*))) #\*))
         ;(write-line "Parsing start")
         `(start ,(parse-expression (subseq l* 0 (1- (length l*)))))))
      ;;add definition
      (#\= (multiple-value-bind (first second)
               (split-string string char)
             `(def ,(parse-expression first) ,(parse-expression second))))
      (#\< (multiple-value-bind (first second)
               (split-string string char)
             `(def-before ,(parse-expression first) ,(parse-expression second))))
      (#\> (multiple-value-bind (first second)
               (split-string string char)
             `(def-after ,(parse-expression first) ,(parse-expression second))))
      (#\/ (multiple-value-bind (first second)
               (split-string string char)
             `(after ,(parse-expression first) ,(parse-expression second))))
      ;;name brackets
      (#\( (multiple-value-bind (first second)
               (split-string string char)
             (let ((count 0)
                   (pos 0))
               (block nil
                 (map nil (lambda (char)
                            (incf pos)
                            (case char
                              (#\( (incf count))
                              (#\) (if (= count 0)
                                       (return)
                                       (decf count)))))
                      second))
               (assert (= count 0) (count) "Brackets don't match")
               (let ((second (subseq second 0 (1- pos)))
                     (third (subseq second pos)))
                 (let ((names
                        (list (parse-expression first)
                              (parse-expression second)
                              (parse-expression third))))
                   `(names ,@(delete-if-not (lambda (s)
                                              (or (atom s) (not (eq (car s) 'empty))))
                                            names)))))))
      ;;multiple alternatives pos
      ((#\| #\Newline)
       (multiple-value-bind (first second)
           (split-string string char)
         (let ((fexp (parse-expression first))
               (sexp (parse-expression second)))
           (setf fexp (if (and (consp fexp)
                               (or (eq (car fexp) 'alt)
                                   (and (eq (car fexp) 'empty)
                                        (char= char #\Newline))))
                          (cdr fexp)
                          (list fexp)))
           (setf sexp (if (and (consp sexp)
                               (or (eq (car sexp) 'alt)
                                   (and (eq (car sexp) 'empty)
                                        (char= char #\Newline))))
                          (cdr sexp)
                          (list sexp)))
           `(alt ,@fexp ,@sexp))))
      ;;multiple parallels
      ((#\+ #\&) (multiple-value-bind (first second)
                     (split-string string char)
                   `(par ,(parse-expression first) ,(parse-expression second))))
      ;;remove comments
      (#\; (parse-expression (split-string string char))))))

(defun parse-expressions (strings)
  `(alt ,@(mapcar #'parse-expression strings)))

(defun get-file (filename)
  (with-open-file (stream filename :if-does-not-exist :create)
    (loop for line = (read-line stream nil)
       while line
       collect line)))

(defun parse-file (filename)
  (parse-expression (format nil "~{~a~%~}" (get-file filename))))


(defun load-expression (string)
  (eval (parse-expression string)))

(defun load-expressions (&rest strings)
  (eval (parse-expressions strings)))

(defun load-file (filename)
  (eval (parse-file filename)))


(defmacro expression (string)
  (parse-expression string))

(defmacro expressions (&body strings)
  (parse-expressions strings))

(defmacro file (filename)
  (parse-file filename))

(defun expand-strings (exp)
  (mapcar (lambda (exp) (if (stringp exp) (parse-expression exp)
                            (if (consp exp)
                                (cons (car exp) (expand-strings (cdr exp)))
                                (if (numberp exp)
                                    exp
                                    (error "Not a valid expression")))))
          exp))

(defmacro with-parsing-strings (&body exp)
  `(progn ,@(expand-strings exp)))

)
