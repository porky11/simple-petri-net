# Other approaches

There may be different approaches for modelling stories. This one seems to be powerful enough to express most things, that may be interesting for modelling stories, but does not allow things, you won't be sure, what happens, if you define it that way.

Here some other approaches will be written, and an explanation, why this was preferred.

Approaches that don't allow as well parallel as alternative story lines won't be covered.

The terminology of petri nets is used when appropiate.

## Extending restrictive approaches

### Extending alternative only

When only having alternative story lines, this means, that the currently played story line is the only state. So after a decision, you can just change the active story line, but if there are multiple ways to get to the same story line, all decisions before are forgotten.

In order to allow this, could set some marker, which can be used later.

*Problems:*
* Only one story line supports alternatives
* Markers cannot be unset, which may allow unwanted things in circular stories, which would be possible using alternatives only by default


### Extending parallel only

When only having parallel story lines, a part would define some conditions, that have to be true, in order to call a part, for example the parts, that already have to be called. Every story part has it's own place, which only itself can consume.

In order to make alternatives, a part could also be defined to depend on other story parts not being called.

*Problems:*
* Circular story lines are not possible
* Depending on something being not finished, cannot added at runtime, since it's a restriction


## Lower level petri net approaches

### Naming places and transitions

This approach directly maps to petri nets.
To define a transition, you give it a list of names, which are the input places, and another list of names, which are the output places.

*Problems:*
* Additional conditions for new parallel story lines are difficult to add
* Defining simple linear stories needs to define a special place for each story part and tell it the story that fills it and the story that consumes from it

### Naming places automatically

To fix the problems from the most direct approach, the transition names depend on the place names.
To define a transition, you only need to add the names of the input places, which could be the transition name itself for the simplest way.

*Problems:*
* Adding parallel story lines, that are alternative to a single story line is still not possible


## More powerful approaches

One restriction of petri nets in the current approach is a bit annoying:
```
a|b: x
b: x|y
```
There is no simple way to model this using petri nets.

Especially, when `x` and `y` never are parallel, there is no problem with this relationship.

The most powerful way to model this is, when `x` and `y` are parallel, you just save multiple states of the petri net, until there is only one way to call it. In this simple case, this is possible, since you see, that `b` can be called anyway, but if you have more complicated things with parallels, this may be very difficult to implement.

Another way is, only to allow this, when you can ensure, that `x` and `y` never are parallel. This may also become complicated for complex stories.

(see my other repository `story-engine`, where I tried to implement a more complex approach)

