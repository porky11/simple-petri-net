# The Language

There is the simple syntax and the s-expression based syntax.

Everytime new syntax is defined, you will see both versions like this:
```
a ;this is a comment in the short syntax
```
```
"a" ;a comment in the s-expression syntax looks the same, since they are just lisp comments
```

### Linear stories

Define new linear stories this way:
```
b: a
```
```
(path "b" "a")
```
The transition `b` consumes a token from a place, that will be created by `a`.
So `b` follows `a`.

When something can be called from beginning, the condition has to be true from beginning.
A condition can be set true from beginning like this:
```
start*
```
```
(start "start")
```

For example if the story starts with intro, you can write this:
```
intro: start*
```

When you want to write down multiple expressions, each of them has to be in a new line:
```
b: a
c: b
```
This defines a longer linear story. Another way to write the same is this:
```
c: b: a
```

### Alternative stories

There may be more than one transition, that depends on the same place:
```
b: a
c: a
```
Both transitions consume the token from the same place.

You can write this instead, which is short for the both expressions before:
```
b|c: a
```
```
(path (alt "b" "c") "a")
```

A single story can also depend on multiple alternative stories:
```
c: a|b
```
In this case, the transition `c` consumes a token from a place, that can be created by `a` and `b`.

A simple use case is this:
```
d: b|c: a
```
After `a` you can choose, if you want to do `b` or `c`. After each of them, you can do `d`.

One relationship, that cannot expressed in petri nets is this:
```
a: b
c: d
a: d
```
Logically the problem only occurs, when `a` and `c` can happen parallel.
When `a` and `c` fire, the place, that `b` needs can only be filled by `a`, but the place, that `d` needs, can be filled by both, `a` and `c`. So the places, `b` and `d` need, cannot be the same, so it cannot describe exactly, what is wanted.
But when trying to implement it differently, there is no deterministic way to calculate, if `b` can still happen, after `d` happened.

Even if there is some way to implement this, it may lead to problems, since the endings `b` and `d` may match the beginnings of `a` and `c` each. So adding `a: d` afterwards may not match that good, and you would need to add some glue.

You can write this, to make it work:
```
a: b
c: d
a: e: d
```

Something like this is also possible:
```
a|b: c|d
```
This will be equivalent to this:
```
a: c
b: c
a: d ;;now the state is invalid
a: d ;;now the state is valid again
```
While a petri net is in invalid state, it's transitions cannot be called.

### Parallel stories

You can also define stories to be parallel. If you want to add new parallel story lines, you just define multiple stories to be parallel:
```
a|b|c: x
x/a&b&c
```
```
(path (alt "a" "b" "c") "x")
(after "x" (par "a" "b" "c"))
```

This can also be written as this:
```
a6b&c: x
```

When you have multiple story lines, they should be able to reunite into a singel story line again.
This can be written as this:

```
x: a&b
```
This does not make `a` and `b` parallel, since something like this is also possible:
```
b&c: a
d|e: b|c ;since `b` and `c` are parallel, you can call d or e each twice now
f: d&e
```

### Brackets and namespaces

You can group expressions using round brackets. This does not affect the s-expression language.
```
(a|b)&c
;; => (par (alt "a" "b") "c") instead of (alt "a" (par "b" "c"))

(a: b): c ;this wont be affected by brackets

a: (b: c)|(d: e)
;; => (path "a" (alt (path "b" "c") (path "d" "e")))

(a: b)|(c: d): e
;; => (path (alt (path "a" "b") (path "c" "d")) "e")
;; => (alt (path "a" "b" "c") (path "c" "d" "e"))
```

Brackets don't have to be closed in the same line.
When multiple lines are between a pair of brackets, they are like alternatives.
```
(
  a:b
  c:d
):e
;;same as:
(a:b)|(c:d):e
```

Brackets can also be used inside names.
```
go (home)
=> go home
```
```
(names "go" "home")
;;=> "go home"
```

Some simple usecase is something like this:
```
answer (yes|no): question
;;the same as this:
answer yes: question
answer no: question
```

This can be used as some kind of namespaces.
```
person (
    say hi
    he also says hi: say hi
    visit: likes you
)
```
This way you don't have to think about long names, when the story gets long.

Some other use case is defining multiple things with similar structure.
```
;;at every place you can look around and sleep. after you did this, you are at the place again.
(village|town|city)(
  : look|sleep: ;;before and after the colons is empty. this means, only the names are used.
  ;;you can also teleport to your home everywhere, but it has to be a different story everywhere
  ;;else it would be a problem
  teleport home: 
)

;;after teleporting you are at home
;;this needs to be different for every place, else you could not leave the place
(
  : (village|town|city) teleport
) home
```

When you want stories to be callable multiple times, you can write this:
```
a(:)
```

It may also be useful to use names from outside a namespace.
This can be done this way:
```
a(.b)
;;same as this:
b
```
```
(names "a" (unnamed (empty) "b"))
;;same as this:
"b"
```

The `.` can be on both sides of a name, and there also can be multiple dots on each side to eat more words, but no additional name is allowed on the other side, so brackets are normally needed:
```
a1 a2 a3 (..b..) c3 c2 c1
;;same as this:
a1 b c1
```

Only a single unnaming for each name is allowed. It's not allowed if two dots of different parts of a name collide:
```
(a.) (.b)
;;not allowed
```

Names and unnaming is even possible for complex expressions:
```
(a|b c) (.d: e)
;;same as:
d: a e
b d: b c e
```


```
village ( ;;multiple actions are possible inside the village
  leave:
  look:
  sleep:
  .person talk:
  : reach from (:) .other place
  : reach from somewhere: .somewhere
)
```

You can also use multiple unnamings at once.
```
place (
  subplace (
    action (a|b|c): ;;subplace actions
    ..global action: ;;a action, that does not start with "place subplace"
  )
)
```

In some cases this will reduce the complexity very much.
For example this:
```
;;for multiple actions, every action depends on a specific condition "not" and a global condition
action (a|b|c) (:not*&.condition)
;;else you would have to write something like this for every action
action a: action a not*&action condition
```

Brackets are also important for comments.
If a round bracket is opened in a comment, it has to be closed again in order to end the comment.
Since you normally don't want single brackets in comments, this should not be a problem.
This way you can have mulit line comments without new syntax and uncommenting whole blocks, without searching for the end.
```
;(
  This is a comment.
  This is still a comment
) Even after the last closing bracket, the comment does not end until newline.

;;now we want to comment a block:
;(
  : (village|town|city) teleport
) home ;;even this is a comment

;;sometimes you may only want to leave the internals of the block, but maybe add it later
;;for example when you defined everything of a chapter in a single namespace
;;in this case, comments may not even be required in some cases
chapter 2 (
  ;;everything that happens in chapter 2
  a: x
  b: y
)
;;the easiest way to remove the prefix "chapter 2"
;chapter 2 ;this does not do anything anyway, so there wouldn't be a problem without commenting it
(
  ;;everything that happens in chapter 2
  a: x
  b: y
)
;;when you are unsure, you can do this
chapter 2
(
  ;;everything that happens in chapter 2
  a: x
  b: y
)

```



### Aliasses

You can add aliasses to a transition identifier like this:
```
x = a
```
```
(def "x" "a")
```

Like transitions, you may add an alias multiple times, and it will be an alternative to it.
For example this also is the same as above:
```
x = a
x = b
c: x
```

The order of the definition can change. You can also add aditional aliasses after defining some relations:
```
x = a|b
c: x
x = d
```

In this case, it may be easier to define `c` multiple times, but if you have many transitions, that need a token in the same transitions, this may be helpful.

It's also possible to add.
Instead of writing this:
```
c: b: a
;;now add an alternative to `b`
c: d: a
```
You can write this:
```
c: b: a
;;now add `b`
b = d
```

Aliasses can also be used to split transitions:
```
c: b: a
b = b-second: b-first
;;you could also write `c: b-second: b-first: a`
```

Another problem that can be solved now is this:
```
c: a|b
d: a
```
You already have defined something, that depends on multiple alternatives, and you want to add something that depends on only one of them:
```
c: a|b
;;now you want to add `d: a`, but it's not allowed
a = e: f
;;now the first expression looks like `c: (e: f)|b
;;this means, `c` needs `e` or `b`
;;everything `a` needed, will be needed by `f` now
;; `e` would identify the ending of `a` and `f` would identify the rest
;;so you can add another ending to `f`, which allows `d`
d: f
```

When the alias should only be used for depending transitions, `<` can be used instead of `=`, when it only should be used for transitions, it depends on, `>` can be used.

