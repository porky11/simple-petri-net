# Known Bugs

`.` does not work correctly in all cases:

`x: (a.) b (.c)` => `x: c`
=> first `b` gets removed from `a.`, then `a` gets removed from `.c`

multiple alternate transitions having the same complex dependencies may not work:

`x|y: a&b|c&d`

# Reporting and fixing bugs

If you find another bug, please report, and I'll either fix them or add them to the list.

If you use this program and have problems with one of the known bugs, report it, too. Then I'll work on fixing them.
The structure of the program is pretty messy, so I don't want to touch the code again, so pull requests for improving and fixing are welcome, too.

