**A more user friendly version of this can be found [here](https://gitlab.com/porky11/pn-editor)**

# What is this?

This is a language for modelling and simulating complex stories, that contain alternative and parallel story lines, composed out of linear parts.
Alternative means, that after one story part, the player can decide, which story can be called afterwards.
Parallel means, that the player can decide, in which order the stories take place.
A decision could be a selection in a list of options, but also some event in a game.
Everytime a transition gets enabled, it will be added to this list or enable this event in the game world, if it gets disabled, it will be removed from the list, or disabled in the game world.

See file `example.pn` for the definition of the petri net for the story,
see `example.story` for the text of the story and it's decisions.
Try to edit it, and test, what is possible.

If something is not possible, it may be not implemented yet, or a restriction (see below).
Normally there should be another way to implement it anyway. Else you may open an issue to discuss, what should be implemented.

## Try it

If you want to try it, you need quicklisp. When quicklisp is installed, just clone this repo into your quicklisp directory (for example `~/quicklisp/local-projects/`) and load it from lisp (`(ql:quickload :simple-petri-net)`).

There are different ways to test it. You need GraphViz installed, because pictures of the petri nets will be generated.

The easiest way to generate a picture use `#'petri-test:generate-image`. This function takes the path to a file, in which a petri net is defined in this langauge (there already are some defined in this repository, they and with `.pn`). If you supply the keyword parameter `:program`, which needs a string, it opens the picture using the supplied string as command.

You also can play the story in command line using the `#'petri-test:interactive` function.
It takes one argument to allow secure function calls and two arguments, which are file paths. The first file is the petri net file. The second file is a story file, which defines some text being displayed, when playing the story. If only one filepath is specified, the extensions `.pn` and `.story` are appended.

### Examples

```
;; quickload
* (ql:quickload :simple-petri-net)

;; Try the example (see example.pn and example.story)
* (petri-test:interactive nil)
;;now you can play the interactive story
;;the first argument is optional
;;it disables the security, and allows more options
;;you can display the current state of the petri net (the story) by writing `:` and an application name for displaying png files like`:feh`, `:eog`, `:viewnior` or whatever you like.

;; Another way to display a story from the lisp REPL:
* (petri-test:generate-image "example.pn" :program (or "feh" "eog" "viewnior" (your-favourite-image-program)))
;; the program is optional
;; the png file will be generated anyway as `petri-net.png`

;; When you want to test another story
* (petri-test:interactive nil "free-world")
```

## Interactive editing

When calling interactive you cannot only play the story, but also interactively edit it.
Write `?` to show some help.
When starting a line with an `!` you can execute expressions of the language.
Show the current state of the world using `:` followed by the image program.
Save the edited story using `*` and optionally the filename.
Stop running using `.`.

### Example session

```
;; Start a new story from beginning
* (petri-test:interactive nil "test")
;;the first argument is the security. It has to be set to `nil` in order to use all features 
This is the intro.

You can do mutliple things after intro.

1: You watch TV the whole day.
2: You go outside and play with your friends.
> !program a game: intro
Added new expressions
Declare story "program a game"
You want to program a game.
So you go to the PC and learn lisp first.
Then you start programming.

1: You watch TV the whole day.
2: You go outside and play with your friends.
3: You want to program a game.
> !alternative intro: alternative start
Added new expressions
You have to write a number from 1 to 3
> !alternative start* ;forgot to set alternative start as a start 
Added new expressions
Declare story "alternative intro"
This is a different story about a different person

1: You watch TV the whole day.
2: You go outside and play with your friends.
3: You want to program a game.
4: This is a different story about a different person
> *test
Saved the story and the petri net to file "test"
You have to write a number from 1 to 4
> 3
So you go to the PC and learn lisp first.

Then you start programming.
```

In order to use the Program you may look at the [user interface](doc/interface.md).

How the language works is documented [here](doc/language.md). The documentation of the language also introduces the concept of the engine more clearly.

There are still a few [bugs](doc/bugs.md).

When you want to know, why this approach was chosen and how other approaches may work, see [this](doc/approaches.md).


